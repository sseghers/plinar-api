const nconf = require("nconf");

nconf.argv()
    .env()
    .file({ file: "server.config.json" });

const ENVIRONMENT = nconf.get("PLINAR_API_ENV");
export default ENVIRONMENT;
