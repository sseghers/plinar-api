const winston = require("winston");

import * as serverCodes from "./../../helpers/server-codes";
import allTopicsConverter from "./../../converters/topics";

function createRequest(req, res, parse){
    const parsedRequest = parse(req);
    if (!parsedRequest) {
        winston.log("error", serverCodes.SERVER_ERROR.message);
        return res.status(serverCodes.SERVER_ERROR.code).json(serverCodes.SERVER_ERROR.message);
    }

    return parsedRequest;
}

function setHeaders(res) {
    res.setHeader('Content-Type', 'application/json');
}

export function GetAll(dataSource) {
    return function(req, res) {
        const parsedRequest = createRequest(req, res, parseGetAll);
        setHeaders(res);

        let session = dataSource.getSession();
        session.run(`match (n:TOPIC) return n order by n.timestamp desc skip {offset} limit {pageSize}`, 
        parsedRequest)
            .then(function(result) {
                session.close();

                const convertedResult = allTopicsConverter(result);
                
                res.status(serverCodes.REQUEST_OK.code).json(convertedResult);
            }).catch(function (error) {
                session.close();
                res.status(serverCodes.SERVER_ERROR.code).json(error);
            });
    }
}

function parseGetAll(request) {
    return {
        offset: request.get("offset") || 0,
        pageSize: request.get("pageSize") || 10
    };
}

export function GetByUid(dataSource) {
    return function(req, res) {
        const parsedRequest = createRequest(req, res, parseGetByUid);
        setHeaders(res);

        let session = dataSource.getSession();
        session.run(`match (n:TOPIC { uid: {uid}}) return *`,  
        parsedRequest)
            .then(function(result) {
                session.close();
                res.status(serverCodes.REQUEST_OK.code).json(result.records);
            }).catch(function (error) {
                session.close();
                res.status(serverCodes.SERVER_ERROR.code).json(error);
            });
    }
}

function parseGetByUid(request) {
    if(request.params.uid === undefined) {
        winston.log("error", "Unable to parse provided url for uid.");
        throw "Error";
    }    

    return {
        uid: convertedUid
    };
}

export function PutByUid(dataSource) {
    return function(req, res) {

        const parsedRequest = createRequest(req, res, parsePutByUid);
        setHeaders(res);

        let session = dataSource.getSession();
        session.run(`merge (newTopic:TOPIC { uid: {uid}, title: {title}, content: {content},
          subtitle: {subtitle}, creator: {creator}, image: {image}, timestamp: {timestamp} }) 
        WITH newTopic return *`,
        parsedRequest)
            .then(function(result) {
                session.close();
                winston.log("info", result);
                const nodesCreated = result.summary.counters._stats.nodesCreated;
                let resultingCode = (nodesCreated)
                    ? serverCodes.RESOURCE_CREATED.code
                    : serverCodes.RESOURCE_UPDATED.code;
                let resultingMessage = (nodesCreated)
                    ? serverCodes.RESOURCE_CREATED.message
                    : serverCodes.RESOURCE_UPDATED.message;
                res.status(resultingCode).json(resultingMessage);
            }).catch(function (error) {
                session.close();
                winston.log("error", error);
                res.status(serverCodes.SERVER_ERROR.code).json(error);
            });
    }
}

function parsePutByUid(request) {
    if (!request.body) {
        return undefined;
    }

    if (!request.body.uid
        || !request.body.title
        || !request.body.timestamp) {
        return undefined;
    }

    const result = {
        uid: request.body.uid,
        title: request.body.title,
        content: request.body.content,
        subtitle: request.body.subtitle || "",
        creator: request.body.creatorUid || "",        
        image: request.body.image || "",
        timestamp: request.body.timestamp
    }

    return result;
}

export function DeleteByUid(dataSource) {
    return function(req, res) {        
        const parsedRequest = createRequest(req, res, parseDeleteByUid);
        setHeaders(res);

        let session = dataSource.getSession();
        session.run(`match (deleteTopic:TOPIC { uid: {uid}})
        match detachDeleteNext = (deleteTopic)-[n:NEXT]->(nextMessage:MESSAGE)
        detach delete detachDeleteNext`, 
        parsedRequest)
            .then(function(result) {
                session.close();
                res.status(serverCodes.REQUEST_OK.code).json(serverCodes.REQUEST_OK.message);
            }).catch(function (error) {
                session.close();
                res.status(serverCodes.SERVER_ERROR.code).json(error);
            });
    }
}

function parseDeleteByUid(request) {
    if (!request.body) {
        return undefined;
    }

    if (!request.body.uid) {
        return undefined;
    }

    const result = {
        uid: request.body.uid
    }

    return result;
}
