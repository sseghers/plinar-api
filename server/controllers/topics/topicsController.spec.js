const sinon = require("sinon");
const should = require("should");

describe("Topics Controller", () => {
    it("should handle a get all request", () => {
        const response = {
            status: sinon.spy(),
            json: sinon.spy()
        };

        const topicsController = require("./topicsController")();
        (topicsController.GetAll())({}, response);
        //[0]-1 First time function is called, [0]-2 First argument
        response.status.calledWith(500).should.equal(true, "Body not present: " + response.status.args[0][0]); 
        response.json.calledWith("There was a problem processing the request.").should.equal(true);
    });
});

// var Book = (book) => { this.save = function(){}};