export default function allTopicsConverter(result) {
  let byUid = {};
  let allUids = [];
  result.records.map(record => {
      const recordProperties = record._fields[0].properties;
      byUid[recordProperties.uid] = {
          uid: recordProperties.uid,
          creator: recordProperties.creator,
          title: recordProperties.title,
          subtitle: recordProperties.subtitle,
          content: recordProperties.content,
          image: recordProperties.image,
          timestamp: recordProperties.timestamp
      };
      allUids.push(recordProperties.uid);
  });
  return {
    topics: {
        byUid,
        allUids
    }
  };
}
