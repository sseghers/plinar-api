const neo4j = require("neo4j-driver").v1;
import ENVIRONMENT from "./../../server.config";

const driver = neo4j
    .driver("bolt://"+ ENVIRONMENT.neo4jHost + ":" + ENVIRONMENT.neo4jPort, 
        neo4j.auth.basic(ENVIRONMENT.neo4jUser, ENVIRONMENT.neo4jPassword));

const Neo4jSource = {
    getSession: function() {
        return driver.session();
    }
}



export default Neo4jSource;
