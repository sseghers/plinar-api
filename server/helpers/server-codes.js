export const SERVER_ERROR = {
    code: 500,
    message: "There was a problem processing the request."
};

export const USER_REQUIRES_AUTHENTICATION = {
    code: 401,
    message: "Authentication credentials must be provided."
};

export const USER_REQUIRES_AUTHORIZATION = {
    code: 403,
    message: "The action is not authorized."
};

export const RESOURCE_CREATED = {
    code: 201,
    message: "Resource successfully created."
};

export const RESOURCE_UPDATED = {
    code: 200,
    message: "Resource successfully updated."
};

export const REQUEST_OK = {
    code: 200,
    message: "The operation was successful."
};