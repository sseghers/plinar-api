import http from "http";
import app from "./server";
import ENVIRONMENT from "./../server.config";

const server = http.createServer(app);
let currentApp = app;
server.listen(ENVIRONMENT.expressPort, ENVIRONMENT.expressHost);

if (module.hot) {
    module.hot.accept("./server", () => {
        server.removeListener("request", currentApp);
        server.on("request", app);
        currentApp = app;
    });
}
