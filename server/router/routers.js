import configureTopicsRoutes from "./topicsRouter";
import Neo4jSource from "./configure-data-source";

const routers = [
    {
        topicsRouter: configureTopicsRoutes(Neo4jSource)
    }
];

export default routers;
