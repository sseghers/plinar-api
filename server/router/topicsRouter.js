import express from "express";
import * as topicsController from "./../controllers/topics/topicsController";

function configureTopicsRoutes(dataSource) {
    const topicsRouter = express.Router();

    topicsRouter.route("/topics")
        .get(topicsController.GetAll(dataSource));

    topicsRouter.route("/topics/:uid")
        .get(topicsController.GetByUid(dataSource))
        .put(topicsController.PutByUid(dataSource))
        .delete(topicsController.DeleteByUid(dataSource));

    return topicsRouter;
}

export default configureTopicsRoutes;
