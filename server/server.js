import express from "express";
const bodyParser = require("body-parser");
const helmet = require("helmet");
const cors = require("cors");

import routers from "./router/routers";
const app = express();

const CORS = (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, Accept, Origin, X-Request-With");
  
  next();
} 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set("json spaces", 2);
app.use(CORS);
app.options("*", cors());
app.use(helmet());
app.use("/api", routers.map(router => router[Object.keys(router)[0]]));

export default app;
