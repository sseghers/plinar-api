const webpack = require("webpack");
const path = require("path");
const nodeExternals = require("webpack-node-externals");
const StartServerPlugin = require("start-server-webpack-plugin");

module.exports = {
    entry: [ "webpack/hot/poll?1000", "./server/index" ],
    watch: true,
    /* In the node target environment, there is no true concept of passing
    a NODE_ENV through, so the process of distinguishing builds per environment 
    will not work. The process.env is artifically made available AFTER webpack has run.
    In order to get the API running through node for heavy debugging, npm run start will
    need to run first to build, then terminate and run through node. Perhaps there's a better approach?
    */
    target: "node",
    externals: [ 
        nodeExternals({ whitelist: [ "webpack/hot/poll?1000" ] }),
        {
            mocha: true
        }
    ],
    module: {
        rules: [
            { 
                test: /\.js?$/, 
                use: "babel-loader", 
                exclude: /node_modules/ },
        ],
    },
    plugins: [
        new StartServerPlugin("server.js"),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            "process.env": { BUILD_TARGET: JSON.stringify("server") },
        })],
    output: { 
        path: path.join(__dirname, ".build"), filename: "server.js" 
    },
};
